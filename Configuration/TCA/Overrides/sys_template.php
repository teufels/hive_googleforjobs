<?php
defined('TYPO3_MODE') or die();

$extKey = 'hive_googleforjobs';
$title = 'HIVE>Google for Jobs';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivegoogleforjobs_domain_model_job', 'EXT:hive_googleforjobs/Resources/Private/Language/locallang_csh_tx_hivegoogleforjobs_domain_model_job.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivegoogleforjobs_domain_model_job');