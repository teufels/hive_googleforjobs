<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 ***************/
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HiveGoogleforjobs',
    'Hivegoogleforjobsrenderstructureddata',
    'Google For Jobs'
);

$extKey = strtolower('hive_googleforjobs');

$pluginName = strtolower('Hivegoogleforjobsrenderstructureddata');
$pluginSignature = str_replace('_', '', $extKey) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extKey . '/Configuration/FlexForms/Config.xml');

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$pluginSignature] = 'hive_googleforjobs_icon';