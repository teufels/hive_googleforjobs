<?php
namespace HIVE\HiveGoogleforjobs\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;

/***
 *
 * This file is part of the "hive_googleforjobs" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

/**
 * JobController
 */
class JobController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * action renderStructureddata
     *
     * @return void
     */
    public function renderStructureddataAction()
    {
        $aSettings = $this->settings;
        $GJTitle = $aSettings['oTitle'];
        $GJDescription = $aSettings['oDescription'];
        $GJEmploymentType = $aSettings['oEmploymentType'];
        $GJHiringOragnization = $aSettings['oHiringOrganization'];
        $GJHiringOragnizationID = $aSettings['oHiringOrganizationID'];
        $GJHiringOragnizationLogo = $aSettings['oHiringOrganizationLogo'];
        $GJHiringOragnizationWebsite = $aSettings['oHiringOrganizationWebsite'];
        $GJJobLocationStreetAddress = $aSettings['oJobLocationStreetAddress'];
        $GJJobLocationCity = $aSettings['oJobLocationCity'];
        $GJJobLocationPostalCode = $aSettings['oJobLocationPostalCode'];
        $GJJobLocationRegion = $aSettings['oJobLocationRegion'];
        $GJJobLocationCountry = $aSettings['oJobLocationCountry'];
        $GJBaseSalaryActive = $aSettings['oBaseSalaryActive'];
        $GJBaseSalaryCurrency = $aSettings['oBaseSalaryCurrency'];
        $GJBaseSalaryValue = $aSettings['oBaseSalaryValue'];
        $GJBaseSalaryUnitText = $aSettings['oBaseSalaryUnitText'];
        $GJRemoteJobActive = $aSettings['oRemoteJobActive'];
        $GJRemoteJobALR = $aSettings['oRemoteJobALR'];
        $GJDatePostedTimestamp = $aSettings['oDatePosted'];
        $GJDatePosted = date("Y-m-d",$GJDatePostedTimestamp);
        $GJValidTroughTimestamp = $aSettings['oValidThrough'];
        $GJValidTrough = date("Y-m-d",$GJValidTroughTimestamp);


        //get absolutePath of $GJHiringOragnizationLogo
        $imageService = GeneralUtility::makeInstance(ImageService::class);
        $imagePath = $imageService->getImage($GJHiringOragnizationLogo,null,false)->getPublicUrl();
        $GJHiringOragnizationLogo_AbsolutePath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" .  $imagePath;

        $GoogleForJobs = array();
        $GoogleForJobs['@context'] = "https://schema.org/";
        $GoogleForJobs['@type'] = 'JobPosting';
        $GoogleForJobs['title'] = $GJTitle;
        $GoogleForJobs['description'] = $GJDescription;

        $GoogleForJobs['image'] = $GJHiringOragnizationLogo_AbsolutePath;
        $GoogleForJobs['employmentType'] = $GJEmploymentType;

        $GoogleForJobs['identifier']['@type'] = 'PropertyValue';
        $GoogleForJobs['identifier']['name'] = $GJHiringOragnization;
        if($GJHiringOragnizationID){
            $GoogleForJobs['identifier']['value'] = $GJHiringOragnizationID;
        }

        $GoogleForJobs['hiringOrganization']['@type'] = 'Organization';
        $GoogleForJobs['hiringOrganization']['name'] = $GJHiringOragnization;
        $GoogleForJobs['hiringOrganization']['sameAs'] = $GJHiringOragnizationWebsite;
        $GoogleForJobs['hiringOrganization']['logo'] = $GJHiringOragnizationLogo_AbsolutePath;

        $GoogleForJobs['jobLocation']['@type'] = 'Place';
        $GoogleForJobs['jobLocation']['address']['@type'] = 'PostalAddress';
        $GoogleForJobs['jobLocation']['address']['streetAddress'] = $GJJobLocationStreetAddress;
        $GoogleForJobs['jobLocation']['address']['addressLocality'] = $GJJobLocationCity;
        $GoogleForJobs['jobLocation']['address']['addressRegion'] = $GJJobLocationRegion;
        $GoogleForJobs['jobLocation']['address']['postalCode'] = $GJJobLocationPostalCode;
        $GoogleForJobs['jobLocation']['address']['addressCountry'] = $GJJobLocationCountry;


        if($GJBaseSalaryActive){
            $GoogleForJobs['baseSalary']['@type'] = 'MonetaryAmount';
            $GoogleForJobs['baseSalary']['currency'] = $GJBaseSalaryCurrency;
            $GoogleForJobs['baseSalary']['value']['@type'] = 'QuantitativeValue';
            $GoogleForJobs['baseSalary']['value']['value'] = $GJBaseSalaryValue;
            $GoogleForJobs['baseSalary']['value']['unitText'] = $GJBaseSalaryUnitText;
        }

        if($GJRemoteJobActive) {
            $GoogleForJobs['jobLocationType'] = 'TELECOMMUTE';
            $GoogleForJobs['applicantLocationRequirements']['@type'] = 'Country';
            $GoogleForJobs['applicantLocationRequirements']['name'] = $GJRemoteJobALR;
        }

        $GoogleForJobs['datePosted'] = $GJDatePosted;
        $GoogleForJobs['validThrough'] = $GJValidTrough;

        $jsonData = json_encode($GoogleForJobs, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        // add script tag
        $sturcturedData = '<script type="application/ld+json">' . $jsonData . '</script>';

        $this->view->assign('data', $GoogleForJobs);
        $this->view->assign('structuredData', $sturcturedData);
    }
}
