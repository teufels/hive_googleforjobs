<?php
namespace HIVE\HiveGoogleforjobs\Domain\Model;

/***
 *
 * This file is part of the "hive_googleforjobs" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

/**
 * Job
 */
class Job extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
}
