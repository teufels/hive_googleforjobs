<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function() {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivegoogleforjobs_domain_model_job', 'EXT:hive_googleforjobs/Resources/Private/Language/locallang_csh_tx_hivegoogleforjobs_domain_model_job.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivegoogleforjobs_domain_model_job');
});
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
