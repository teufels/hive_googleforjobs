<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function() {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'HiveGoogleforjobs',
        'Hivegoogleforjobsrenderstructureddata',
        [HIVE\HiveGoogleforjobs\Controller\JobController::class => 'renderStructureddata'],
        // non-cacheable actions
        [HIVE\HiveGoogleforjobs\Controller\JobController::class => '']
    );

    // Register content element icons
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'hive_googleforjobs_icon',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:hive_googleforjobs/Resources/Public/Icons/hive_32x32.svg',
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins.elements.hivegoogleforjobsrenderstructureddata >
            wizards.newContentElement.wizardItems {
                hive {
                    header = Hive
                    after = common,special,menu,plugins,forms
                    elements.hivegoogleforjobsrenderstructureddata {
                        iconIdentifier = hive_googleforjobs_icon
                        title = Google for Jobs
                        description = Render Google for Jobs Structured Data
                        tt_content_defValues {
                             CType = list
                             list_type = hivegoogleforjobs_hivegoogleforjobsrenderstructureddata
                        }
                    }
                    show := addToList(hivegoogleforjobsrenderstructureddata)
                }

            }
        }'
    );

});